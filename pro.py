import sys
from PyQt5.QtWidgets import (QTableWidget, QTableWidgetItem, QTextEdit, QLineEdit, QLabel, QPushButton, QSlider, QVBoxLayout, QHBoxLayout, QApplication, QWidget, QCheckBox, QMainWindow, QStackedLayout, QStackedWidget, QComboBox, QDialog, QDialogButtonBox, QMessageBox, QTreeWidget, QTreeWidgetItem, QTabWidget)
from PyQt5.QtCore import Qt
import fdb

class Main(QMainWindow):

	def __init__(self):
		super().__init__()
		self.init_ui()

	def init_ui(self):
		self.cons = ConnectionView()
		self.objects = ObjectView()

		self.setFixedSize(1280, 720)
		self.center = QWidget()
		self.sl = QStackedWidget()
		#self.center.setLayout(self.sl)
		self.setCentralWidget(self.sl)

		self.sl.addWidget(self.cons)
		self.sl.addWidget(self.objects)
		
		self.sl.setCurrentIndex(0)
		self.setWindowTitle('TBD2')

		self.show()

	def connect(self, connection):
		
		self.objects.connection = connection
		try:
			self.objects.tryConnect()
		except fdb.fbcore.DatabaseError as e:
			error = QMessageBox()
			error.setText(str(e))
			error.setWindowTitle('Error')
			error.setIcon(QMessageBox.Critical)
			error.exec_()
			return
		self.objects.loadTree()
		self.sl.setCurrentIndex(1)


class Cons:
	def __init__(self, name = '', user = '', password = '', ip = '', filename = ''):
		self.name = name
		self.user = user
		self.password = password
		self.ip = ip
		self.filename = filename

	def fromStr(self, string):
		load = string.split(';')
		self.name = load[0]
		self.user = load[1]
		self.password = load[2]
		self.ip = load[3]
		self.filename = load[4]

	def toStr(self):
		string = self.name + ';' + self.user + ';' + self.password + ';' + self.ip + ';' + self.filename + '~'
		return string

class ConnectionView(QWidget):

	def __init__(self):
		super().__init__()
		self.init_ui()

	def init_ui(self):

		self.connList = [];

		self.titulo = QLabel('Connections')
		h_box_tit = QHBoxLayout()
		h_box_tit.addStretch()
		h_box_tit.addWidget(self.titulo)
		h_box_tit.addStretch()

		self.combo = QComboBox()
		h_box_combo = QHBoxLayout()
		h_box_combo.addStretch()
		h_box_combo.addWidget(self.combo)
		h_box_combo.addStretch()

		self.newCon_b = QPushButton('New Connection')
		self.connect_b = QPushButton('Connect')
		self.edit_b = QPushButton('Edit')
		self.delete_b = QPushButton('Delete')
		h_box_buttons = QHBoxLayout()
		h_box_buttons.addStretch()
		h_box_buttons.addWidget(self.newCon_b)
		h_box_buttons.addWidget(self.connect_b)
		h_box_buttons.addWidget(self.edit_b)
		h_box_buttons.addWidget(self.delete_b)
		h_box_buttons.addStretch()

		v_box = QVBoxLayout()
		v_box.addStretch()
		v_box.addLayout(h_box_tit)
		v_box.addLayout(h_box_combo)
		v_box.addLayout(h_box_buttons)
		v_box.addStretch()

		self.loadComboBox()

		self.newCon_b.clicked.connect(self.newConnection)
		self.connect_b.clicked.connect(self.connect)
		self.delete_b.clicked.connect(self.deleteConnection)
		self.edit_b.clicked.connect(self.editConnection)

		self.setLayout(v_box)
		self.show()

	def loadComboBox(self):
		self.combo.clear()
		self.loadConnections()
		for con in self.connList:
			self.combo.addItem(con.name)

	def loadConnections(self):
		file = open('connections.txt', 'r')
		self.connList = []
		string = file.read()
		load = string.split('~')
		for con in load:
			if(con == ''):
				break
			newCon = Cons()
			newCon.fromStr(con)
			self.connList.append(newCon)
		file.close()

	def writeConnections(self):
		file = open('connections.txt', 'w')
		for conn in self.connList:
			file.write(conn.toStr())
		file.close()

	def newConnection(self):
		conn, result = NewConnection.newConnection()
		if(result):
			if(conn.name == '' or conn.user == '' or conn.password == '' or conn.ip == '' or conn.filename == ''):
				error = QMessageBox()
				error.setText('Error: connection attribute cannot be blank')
				error.setWindowTitle('Error')
				error.setIcon(QMessageBox.Critical)
				error.exec_()
				return
			self.connList.append(conn)
			self.writeConnections()
			self.loadComboBox()
			w.connect(conn)

	def editConnection(self):
		if(self.combo.count() == 0):
			error = QMessageBox()
			error.setText('Error: No connection selected')
			error.setWindowTitle('Error')
			error.setIcon(QMessageBox.Critical)
			error.exec_()
			return
		conn = self.findConn(self.combo.currentText())
		conn2, result = NewConnection.editConnection(conn)
		if(result):
			if(conn.name == '' or conn.user == '' or conn.password == '' or conn.ip == '' or conn.filename == ''):
				error = QMessageBox()
				error.setText('Error: connection attribute cannot be blank')
				error.setWindowTitle('Error')
				error.setIcon(QMessageBox.Critical)
				error.exec_()
				return
			self.connList.remove(conn)
			self.connList.append(conn2)
			self.writeConnections()
			self.loadComboBox()

	def deleteConnection(self):
		if(self.combo.count() == 0):
			error = QMessageBox()
			error.setText('Error: No connection selected')
			error.setWindowTitle('Error')
			error.setIcon(QMessageBox.Critical)
			error.exec_()
			return

		buttonReply = QMessageBox.question(self, 'Delete Connection', "Are you sure you want to delete the connection?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
		if buttonReply != QMessageBox.Yes:
			return

		self.connList.remove(self.findConn(self.combo.currentText()))
		self.writeConnections()
		self.loadComboBox()

	def findConn(self, name):
		conn = 0
		for con in self.connList:
			if(con.name == name):
				return con

	def connect(self):
		if(self.combo.count() == 0):
			error = QMessageBox()
			error.setText('Error: No connection selected')
			error.setWindowTitle('Error')
			error.setIcon(QMessageBox.Critical)
			error.exec_()
			return
		conn = self.findConn(self.combo.currentText())
		w.connect(conn)

class NewConnection(QDialog):
	def __init__(self):
		super(NewConnection, self).__init__()
		self.setWindowTitle('New Connection')
		self.setFixedSize(300, 175)

		
		connName_l = QLabel('Connection name: ')
		self.connName_e = QLineEdit()
		self.connName_e.setFixedSize(150, 20)
		h_box_connName = QHBoxLayout()
		h_box_connName.addStretch()
		h_box_connName.addWidget(connName_l)
		h_box_connName.addWidget(self.connName_e)
		#h_box_connName.addStretch()

		connUser_l = QLabel('User: ')
		self.connUser_e = QLineEdit()
		self.connUser_e.setFixedSize(150, 20)
		h_box_connUser = QHBoxLayout()
		h_box_connUser.addStretch()
		h_box_connUser.addWidget(connUser_l)
		h_box_connUser.addWidget(self.connUser_e)
		#h_box_connUser.addStretch()

		connPass_l = QLabel('Password: ')
		self.connPass_e = QLineEdit()
		self.connPass_e.setFixedSize(150, 20)
		h_box_connPass = QHBoxLayout()
		h_box_connPass.addStretch()
		h_box_connPass.addWidget(connPass_l)
		h_box_connPass.addWidget(self.connPass_e)
		#h_box_connPass.addStretch()

		connIP_l = QLabel('IP: ')
		self.connIP_e = QLineEdit()
		self.connIP_e.setFixedSize(150, 20)
		h_box_connIP = QHBoxLayout()
		h_box_connIP.addStretch()
		h_box_connIP.addWidget(connIP_l)
		h_box_connIP.addWidget(self.connIP_e)
		#h_box_connIP.addStretch()

		connFile_l = QLabel('File name: ')
		self.connFile_e = QLineEdit()
		self.connFile_e.setFixedSize(150, 20)
		h_box_connFile = QHBoxLayout()
		h_box_connFile.addStretch()
		h_box_connFile.addWidget(connFile_l)
		h_box_connFile.addWidget(self.connFile_e)
		#h_box_connFile.addStretch()

		v_box = QVBoxLayout()
		v_box.addLayout(h_box_connName)
		v_box.addLayout(h_box_connUser)
		v_box.addLayout(h_box_connPass)
		v_box.addLayout(h_box_connIP)
		v_box.addLayout(h_box_connFile)

		buttons = QDialogButtonBox(
	        QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
	        Qt.Horizontal, self)
		buttons.accepted.connect(self.accept)
		buttons.rejected.connect(self.reject)
		v_box.addWidget(buttons)

		self.setLayout(v_box)

	def getConnection(self):
		conn = Cons(self.connName_e.text(), self.connUser_e.text(), password = self.connPass_e.text(), ip = self.connIP_e.text(), filename = self.connFile_e.text())
		return conn

	@staticmethod
	def newConnection():
		dialog = NewConnection()
		result = dialog.exec_()
		conn = dialog.getConnection()
		return (conn, result == QDialog.Accepted)

	@staticmethod
	def editConnection(conn):
		dialog = NewConnection()
		dialog.connName_e.setText(conn.name)
		dialog.connUser_e.setText(conn.user)
		dialog.connPass_e.setText(conn.password)
		dialog.connIP_e.setText(conn.ip)
		dialog.connFile_e.setText(conn.filename)
		result = dialog.exec_()
		conn = dialog.getConnection()
		return (conn, result == QDialog.Accepted)



class ObjectView(QWidget):

	def __init__(self):
		super().__init__()
		self.init_ui()

	def init_ui(self):
		self.connection = 0;
		
		self.titulo = QLabel()
		h_box_tit = QHBoxLayout()
		h_box_tit.addStretch()
		h_box_tit.addWidget(self.titulo)
		h_box_tit.addStretch()

		self.tree = QTreeWidget()
		self.tree.setFixedSize(270, 580)
		v_box_left = QVBoxLayout()
		v_box_left.addStretch()
		v_box_left.addLayout(h_box_tit)
		v_box_left.addWidget(self.tree)
		v_box_left.addStretch()

		self.tabs = QTabWidget()
		self.sql = QTextEdit()
		self.results = QTableWidget()
		self.tabs.addTab(self.sql, 'SQL')
		self.tabs.addTab(self.results, 'Results')
		self.tabs.setFixedSize(970, 600)
		self.run_b = QPushButton('Run')
		h_box_buttons = QHBoxLayout()
		h_box_buttons.addStretch()
		h_box_buttons.addWidget(self.run_b)
		v_box_right = QVBoxLayout()
		v_box_right.addWidget(self.tabs)
		v_box_right.addLayout(h_box_buttons)

		h_box = QHBoxLayout()
		h_box.addLayout(v_box_left)
		h_box.addLayout(v_box_right)

		self.run_b.clicked.connect(lambda: self.callSQL(self.sql.toPlainText()))

		self.setLayout(h_box)
		self.show()

	def callSQL(self, sql):
		con = fdb.connect(dsn= self.connection.ip + r':C:\Users\Diegu Montes\Desktop\database IB\\' + self.connection.filename, user=self.connection.user, password=self.connection.password)
		cur = con.cursor()
		cur.execute(sql)
		i = 0
		res = cur.fetchall()
		rownum = len(res)
		self.tabs.setCurrentIndex(1)
		if(rownum == 0):
			con.close()
			return
		colnum = len(res[0])
		
		self.results.setColumnCount(colnum)
		self.results.setRowCount(rownum)
		for row in res:
			j = 0
			while(j < colnum):
				self.results.setItem(i, j, QTableWidgetItem(str(row[j])))
				j = j + 1
			i = i + 1
		
		con.close()


	def tryConnect(self):
		con = fdb.connect(dsn= self.connection.ip + r':C:\Users\Diegu Montes\Desktop\database IB\\' + self.connection.filename, user=self.connection.user, password=self.connection.password)
		name = con.database_info(fdb.isc_info_db_id, 's')
		name = self.connection.filename
		self.titulo.setText(str(name))
		con.close()

	def loadTree(self):
		con = fdb.connect(dsn= self.connection.ip + r':C:\Users\Diegu Montes\Desktop\database IB\\' + self.connection.filename, user=self.connection.user, password=self.connection.password)
		cur = con.cursor()

		self.tree.setHeaderLabel('Objects')

		self.tables = QTreeWidgetItem(self.tree)
		self.tables.setText(0, 'Tables')
		cur.execute('SELECT RDB$RELATION_NAME as TABLES FROM RDB$RELATIONS WHERE RDB$SYSTEM_FLAG=0 and RDB$RELATION_TYPE=\'PERSISTENT\';')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.tables)
			obj.setText(0, objectname)

		self.indexes = QTreeWidgetItem(self.tree)
		self.indexes.setText(0, 'Indexes')
		cur.execute('SELECT RDB$INDEX_NAME as index_name, RDB$RELATION_NAME as table_name FROM RDB$INDICES where rdb$index_type=0;')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.indexes)
			obj.setText(0, objectname)

		self.funs = QTreeWidgetItem(self.tree)
		self.funs.setText(0, 'Procedures')
		cur.execute('SELECT RDB$PROCEDURE_NAME as procedure_name, rdb$owner_name as owner FROM RDB$PROCEDURES;')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.funs)
			obj.setText(0, objectname)

		self.trigs = QTreeWidgetItem(self.tree)
		self.trigs.setText(0, 'Triggers')
		cur.execute('SELECT RDB$TRIGGER_NAME as trigger_name, RDB$RELATION_NAME as table_name FROM RDB$TRIGGERS WHERE RDB$SYSTEM_FLAG=0;')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.trigs)
			obj.setText(0, objectname)

		self.views = QTreeWidgetItem(self.tree)
		self.views.setText(0, 'Views')
		cur.execute('SELECT DISTINCT RDB$VIEW_NAME as views FROM RDB$VIEW_RELATIONS;')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.views)
			obj.setText(0, objectname)

		self.checks = QTreeWidgetItem(self.tree)
		self.checks.setText(0, 'Checks')
		cur.execute('select r.RDB$CONSTRAINT_NAME as check_NAME, r.RDB$RELATION_NAME as table_name from rdb$relation_constraints r where r.rdb$constraint_type=\'CHECK\';')
		for row in cur.fetchall():
			objectname = row[0]
			obj = QTreeWidgetItem(self.checks)
			obj.setText(0, objectname)

		con.close()


app = QApplication(sys.argv)
w = Main()
sys.exit(app.exec_())

 